import unittest
import requests
import json

class TestVectorAPIEndpoints(unittest.TestCase):

  def setUp(self):
    self.host = "localhost"
    self.port = "8088"
    self.version = "v1"
    self.vectors_path = "vectors"
    self.words_path = "words"
    self.algebra_path = "algebra"

  def test_get_word_vector(self):
    word = "banana"
    response = requests.get("http://{}:{}/{}/{}/{}".format(
      self.host,
      self.port,
      self.version,
      self.vectors_path,
      word
    ))
    expected_code = 200
    self.assertEqual(response.status_code, expected_code)
    result = json.loads(response.content)
    self.assertIn("status", result)
    self.assertEqual(result["status"], expected_code)
    self.assertIn("data", result)
    self.assertEqual(len(result["data"]), 1)

    first_result = result["data"][0]
    self.assertIn("word", first_result)
    self.assertIn("vector", first_result)
    self.assertIn("vector_2d", first_result)
    self.assertEqual(first_result["word"], word)

  def test_get_word_vector_exception(self):
    word = "king33ns7queen"
    response = requests.get("http://{}:{}/{}/{}/{}".format(
      self.host,
      self.port,
      self.version,
      self.vectors_path,
      word
    ))
    self.assertEqual(response.status_code, 400)
    result = json.loads(response.content)
    self.assertIn("message", result)
    self.assertIn(word, result["message"])

  def test_get_multiple_word_vectors(self):
    words = "king,queen,prince"
    response = requests.get(
      "http://{}:{}/{}/{}/{}".format(
        self.host,
        self.port,
        self.version,
        self.vectors_path, words))
    expected_code = 200
    self.assertEqual(response.status_code, expected_code)
    result = json.loads(response.content)
    
  def test_missing_field(self):
    response = self.__make_api_get_request(path=self.vectors_path)
    self.assertEqual(response.status_code, 400)
    result = json.loads(response.content)
    self.assertIn("message", result)

  def test_get_all_words(self):
    response = self.__make_api_get_request(path=self.words_path)
    expected_code = 200
    self.assertEqual(response.status_code, expected_code)
    result = json.loads(response.content)
    self.assertIn("words", result)
    self.assertGreaterEqual(len(result["words"]),
                            1,
                            "Model should not have empty vocabulary.")

  def test_algebra_missing_params(self):
    response = self.__make_api_get_request(path=self.algebra_path)
    self.assertEqual(response.status_code, 400)

  def test_algebra_add(self):
    response = self.__make_api_get_request(
      path=self.algebra_path,
      params={"add": "man,woman"}
    )
    expected_code = 200
    self.assertEqual(response.status_code, expected_code)
    result = json.loads(response.content)
    self.assertIn("add", result)
    self.assertListEqual(result["add"], ["man", "woman"])
    self.assertIn("subtract", result)
    self.assertListEqual(result["subtract"], [])

  def test_algebra_subtract(self):
    response = self.__make_api_get_request(
      path=self.algebra_path,
      params={"subtract": "man,woman"}
    )
    expected_code = 200
    self.assertEqual(response.status_code, expected_code)
    result = json.loads(response.content)
    self.assertIn("subtract", result)
    self.assertListEqual(result["subtract"], ["man", "woman"])
    self.assertIn("add", result)
    self.assertListEqual(result["add"], [])

  def test_algebra_subtract_add(self):
    response = self.__make_api_get_request(
      path=self.algebra_path,
      params={"subtract": "man", "add": "neo,woman"}
    )
    expected_code = 200
    self.assertEqual(response.status_code, expected_code)
    result = json.loads(response.content)
    self.assertIn("result", result)
    self.assertIn("subtract", result)
    self.assertListEqual(result["subtract"], ["man"])
    self.assertIn("add", result)
    self.assertListEqual(result["add"], ["neo", "woman"])

  def test_algebra_top_three(self):
    response = self.__make_api_get_request(path=self.algebra_path,
      params={"subtract": "man",
              "add": "neo,woman",
              "top": 3})
    expected_code = 200
    self.assertEqual(response.status_code, expected_code)
    result = json.loads(response.content)
    self.assertIn("result", result)
    self.assertEqual(
      len(result["result"]),
      3,
      "Expected three results, got {}".format(result["result"]))

  def test_algebra_top_exceptions(self):
    top_values = ["0", "1.2", "1.", "a", "abed3d", "-100", "111", "1e3", "1e-3"]
    responses = [self.__make_api_get_request(path=self.algebra_path,
                                           params={"subtract": "man",
                                                   "add": "neo,woman",
                                                   "top": value})
                 for value in top_values]
    expected_code = 400
    [self.assertEqual(response.status_code,
                      expected_code,
                      "Expected '400 Bad Request' for 'top' value '{}'. "
                      "Got '{}'.".format(top_values[i], response.status_code))
     for i, response in enumerate(responses)]

  def test_algebra_exception(self):
    response = self.__make_api_get_request(
      path=self.algebra_path,
      params={"subtract": "king", "add": "neowoman224"}
    )
    expected_code = 400
    self.assertEqual(response.status_code, expected_code)
    result = json.loads(response.content)
    self.assertIn("description", result)
    self.assertIn("neowoman224", result["description"])
    self.assertNotIn("king", result["description"])


  def test_algebra_multiple_exception(self):
    response = self.__make_api_get_request(
      path=self.algebra_path,
      params={"subtract": "king", "add": "neowoman224,bsdhy3de"}
    )
    expected_code = 400
    self.assertEqual(response.status_code, expected_code)
    result = json.loads(response.content)
    self.assertIn("description", result)
    self.assertIn("neowoman224",
                  result["description"],
                  "Invalid words expected in error message")
    self.assertIn("bsdhy3de",
                  result["description"],
                  "Invalid words expected in error message")
    self.assertNotIn("king",
                     result["description"],
                     "Valid word should not be in error message")

  def __make_api_get_request(self, path, params=None):
    return requests.get("http://{}:{}/{}/{}".format(
      self.host,
      self.port,
      self.version,
      path
    ), params=params)

if __name__ == '__main__':
    unittest.main()