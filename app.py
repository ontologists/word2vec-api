import json
import logging
from logging.config import fileConfig

import falcon
from falcon_cors import CORS
from sklearn import decomposition

from w2v import *

class VectorResource:
  def on_get(self, req, resp, words):
    """Handles GET Requests"""

    words = words.split(',')
    vectors = [get_word_vector(word)
               for word in words]
    none_vectors = [words[i]
                    for i, vector in enumerate(vectors) if vector is None]
    if none_vectors:
      resp.status = falcon.HTTP_400
      resp.body = json.dumps({
        "status": 400,
        "message": "Words {} not found in model vocabulary".format(
          none_vectors)
      })
    else:
      json_struct = [{"word": word,
                      "vector": json.dumps(vectors[i].tolist(),
                                           separators=(',', ':')),
                      "vector_2d": json.dumps(
                        pca.transform(
                          vectors[i].reshape(1, -1)).tolist()[0],
                        separators=(',', ':')) }
                     for i, word in enumerate(words)]

      resp.body = json.dumps({
        "status": 200,
        "data": json_struct
      })

class AllWordsResource:
  def on_get(self, req, resp):
    resp.body = json.dumps({
      "status": 200,
      "words": list(index2word_set)
    })

class WordAlgebraResource:
  # def __init__(self):
    # fileConfig('logging_config.ini')
    # self.logger = logging.getLogger("algebra")


  def on_get(self, req, resp):
    params = {
      "add": req.get_param(name='add',
                           required=False),
      "subtract": req.get_param(name='subtract',
                                required=False)}
    params["top"] = req.get_param_as_int(name='top',
                                         required=False,
                                         min=1,
                                         max=25) or 1

    if params["add"] is None and params["subtract"] is None:
      raise falcon.HTTPBadRequest(description="Expected params 'add' or "
                                  "'subtract' or both to be present, got none")

    # self.logger.info(params)
    params["add"] = [] if params["add"] is None else params["add"].split(',')
    params["subtract"] = [] if params["subtract"] is None else params["subtract"].split(',')
    # self.logger.info(params)

    try:
      result = word_algebra(add=params["add"],
                            subtract=params["subtract"],
                            topn=params["top"])
      resp.body = json.dumps({
        "add": params["add"],
        "subtract": params["subtract"],
        "result": result
      })
    except ValueError as error:
      raise falcon.HTTPBadRequest(description=str(error))
    # self.logger.info("Result: {}".format(result))



class MissingFieldSinkAdapter:
  def __call__(self, req, resp):
    resp.status = falcon.HTTP_400
    resp.body = json.dumps({
      "status": 400,
      "message": "Missing expected field 'word'. "
        "Must provide field in URL. "
        "URL should take the form '/vectors/<word>"
    })


pca = decomposition.PCA(n_components=2)
print("Fitting PCA to model...")
pca.fit(model.syn0)
print("PCA fitting complete.")
version = "v1"

cors_allow_all = CORS(allow_all_origins=True,
                      allow_all_headers=True,
                      allow_all_methods=True)


api = falcon.API(middleware=[cors_allow_all.middleware])
api.add_sink(MissingFieldSinkAdapter(), "/" + version + "/vectors")

api.add_route("/" + version + "/vectors/{words}", VectorResource())
api.add_route("/" + version + "/words", AllWordsResource())
api.add_route("/" + version + "/algebra", WordAlgebraResource())

